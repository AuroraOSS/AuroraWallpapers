/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.manager;

import android.content.Context;

import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.model.Wall;
import com.aurora.wallpapers.utils.PrefUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FavouritesManager {

    private final HashMap<String, Wall> hashMap = new HashMap<>();

    private Context context;
    private Gson gson;

    public FavouritesManager(Context context) {
        this.context = context;
        this.gson = new Gson();
        this.hashMap.putAll(getDefaultWallMap());
    }

    public List<Wall> getAllWallList() {
        return new ArrayList<>(hashMap.values());
    }

    public boolean addToWallMap(Wall wall) {
        synchronized (hashMap) {
            if (hashMap.containsKey(wall.getId())) {
                return false;
            } else {
                hashMap.put(wall.getId(), wall);
                saveWallMap();
                return true;
            }
        }
    }

    public boolean isFav(String wallId) {
        return hashMap.containsKey(wallId);
    }

    public Wall getWallById(String walId) {
        synchronized (hashMap) {
            if (hashMap.containsKey(walId))
                return hashMap.get(walId);
            else
                return new Wall();
        }
    }

    public void removeFromWallMap(Wall wall) {
        synchronized (hashMap) {
            hashMap.remove(wall.getId());
            saveWallMap();
        }
    }

    private void saveWallMap() {
        synchronized (hashMap) {
            PrefUtil.putString(context, Constants.PREFERENCE_FAVOURITES, gson.toJson(hashMap));
        }
    }

    public void clear() {
        synchronized (hashMap) {
            hashMap.clear();
            saveWallMap();
        }
    }

    private HashMap<String, Wall> getDefaultWallMap() {
        final String rawList = PrefUtil.getString(context, Constants.PREFERENCE_FAVOURITES);
        final Type type = new TypeToken<HashMap<String, Wall>>() {
        }.getType();

        final HashMap<String, Wall> wallHashMap = gson.fromJson(rawList, type);

        if (wallHashMap == null || wallHashMap.isEmpty())
            return new HashMap<>();
        else
            return wallHashMap;
    }
}
