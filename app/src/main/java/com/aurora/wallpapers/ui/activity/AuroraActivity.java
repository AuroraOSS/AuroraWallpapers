/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.aurora.wallpapers.BuildConfig;
import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.R;
import com.aurora.wallpapers.model.Update;
import com.aurora.wallpapers.retro.RetroClient;
import com.aurora.wallpapers.retro.UpdateService;
import com.aurora.wallpapers.ui.sheet.FilterSheet;
import com.aurora.wallpapers.ui.sheet.UpdateSheet;
import com.aurora.wallpapers.utils.Log;
import com.aurora.wallpapers.utils.ViewUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import org.apache.commons.lang3.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuroraActivity extends BaseActivity {

    @BindView(R.id.nav_view)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.navigation)
    NavigationView navigation;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.action1)
    AppCompatImageView action1;
    @BindView(R.id.search_bar)
    RelativeLayout searchBar;
    @BindView(R.id.action2)
    ImageView action2;
    @BindView(R.id.search_edit_text)
    AppCompatEditText searchEditText;

    private CompositeDisposable disposable = new CompositeDisposable();

    static boolean matchDestination(@NonNull NavDestination destination, @IdRes int destId) {
        NavDestination currentDestination = destination;
        while (currentDestination.getId() != destId && currentDestination.getParent() != null) {
            currentDestination = currentDestination.getParent();
        }
        return currentDestination.getId() == destId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupNavigation();
        setupDrawer();
        setupSearch();

        setupSelfUpdate();

        checkPermissions();
    }

    @Override
    protected void onDestroy() {
        disposable.dispose();
        super.onDestroy();
    }

    @OnClick(R.id.filter_fab)
    public void showFilterDialog() {
        if (getSupportFragmentManager().findFragmentByTag(FilterSheet.TAG) == null) {
            final FilterSheet filterSheet = new FilterSheet();
            filterSheet.show(getSupportFragmentManager(), FilterSheet.TAG);
        }
    }

    private void setupNavigation() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);

        //Avoid Adding same fragment to NavController, if clicked on current BottomNavigation item
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            if (item.getItemId() == bottomNavigationView.getSelectedItemId())
                return false;
            NavigationUI.onNavDestinationSelected(item, navController);
            return true;
        });

        //Check correct BottomNavigation item, if navigation_main is done programmatically
        navController.addOnDestinationChangedListener((controller, destination, arguments) -> {
            final Menu menu = bottomNavigationView.getMenu();
            final int size = menu.size();
            for (int i = 0; i < size; i++) {
                MenuItem item = menu.getItem(i);
                if (matchDestination(destination, item.getItemId())) {
                    item.setChecked(true);
                }
            }
        });
    }

    private void setupDrawer() {
        action1.setOnClickListener(v -> {
            if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START, true);
        });

        navigation.setNavigationItemSelectedListener(item -> {
            Intent intent = new Intent(this, GenericActivity.class);
            switch (item.getItemId()) {
                case R.id.action_favourite:
                    intent.putExtra(Constants.FRAGMENT_NAME, Constants.FRAGMENT_FAVOURITE);
                    startActivity(intent, ViewUtil.getEmptyActivityBundle(this));
                    break;
                case R.id.action_about:
                    intent.putExtra(Constants.FRAGMENT_NAME, Constants.FRAGMENT_ABOUT);
                    startActivity(intent, ViewUtil.getEmptyActivityBundle(this));
                    break;
                case R.id.action_setting:
                    startActivity(new Intent(this, SettingsActivity.class),
                            ViewUtil.getEmptyActivityBundle(this));
                    break;
                case R.id.action_bing:
                    intent.putExtra(Constants.FRAGMENT_NAME, Constants.FRAGMENT_BING);
                    startActivity(intent, ViewUtil.getEmptyActivityBundle(this));
                    break;
            }
            return false;
        });
    }

    private void setupSearch() {

        action2.setOnClickListener(v -> {
            searchEditText.setText("");
        });

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (StringUtils.isNotEmpty(s)) {
                    action2.setImageDrawable(getDrawable(R.drawable.ic_cancel));
                } else
                    action2.setImageDrawable(getDrawable(R.drawable.ic_round_search));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        searchEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                String query = searchEditText.getText().toString();
                if (!query.isEmpty()) {
                    openSearchResultActivity(query);
                    return true;
                }
            }
            return false;
        });
    }

    private void setupSelfUpdate() {
        UpdateService downloadService = RetroClient.getInstance().create(UpdateService.class);
        Call<Update> call = downloadService.checkUpdate(Constants.UPDATE_URL);
        call.enqueue(new Callback<Update>() {
            @Override
            public void onResponse(Call<Update> call, Response<Update> response) {
                if (response.isSuccessful()) {
                    final Update update = response.body();
                    if (update != null && update.getVersionCode() > BuildConfig.VERSION_CODE) {
                        showUpdateSheet(update);
                    } else {
                        Log.d(getString(R.string.update_unavailable));
                    }
                } else {
                    Log.d(getString(R.string.update_failed));
                }
            }

            @Override
            public void onFailure(Call<Update> call, Throwable t) {
                Toast.makeText(AuroraActivity.this, getString(R.string.update_failed),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showUpdateSheet(Update update) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.findFragmentByTag(UpdateSheet.TAG) == null) {
            final UpdateSheet updateSheet = new UpdateSheet();
            final Bundle bundle = new Bundle();
            bundle.putString(Constants.STRING_EXTRA, new Gson().toJson(update));
            updateSheet.setArguments(bundle);
            fragmentManager
                    .beginTransaction()
                    .add(updateSheet, UpdateSheet.TAG)
                    .commitAllowingStateLoss();
        }
    }

    private void openSearchResultActivity(String query) {
        Intent intent = new Intent(this, SearchActivity.class);
        intent.putExtra(Constants.STRING_EXTRA, query);
        startActivity(intent, ViewUtil.getEmptyActivityBundle(this));
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1337);
        }
    }
}
