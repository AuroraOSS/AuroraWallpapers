/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.ui.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.aurora.wallpapers.model.wallhaven.Info;
import com.aurora.wallpapers.model.wallhaven.WallInfo;
import com.aurora.wallpapers.retro.RetroClient;
import com.aurora.wallpapers.retro.WallHavenService;
import com.aurora.wallpapers.utils.Util;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WallInfoViewModel extends AndroidViewModel {

    private MutableLiveData<WallInfo> data = new MutableLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public WallInfoViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<WallInfo> getData() {
        return data;
    }

    public void fetchData(String id) {
        WallHavenService service = RetroClient.getInstance().create(WallHavenService.class);
        Call<Info> call = service.getWallInfo(id, Util.getWallHavenAPIKey(getApplication()));

        call.enqueue(new Callback<Info>() {
            @Override
            public void onResponse(Call<Info> call, Response<Info> response) {
                if (response.body() != null) {
                    final Info info = response.body();
                    data.setValue(info.getData());
                } else
                    data.setValue(null);
            }

            @Override
            public void onFailure(Call<Info> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    protected void onCleared() {
        disposable.dispose();
        super.onCleared();
    }
}