/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.ui.sheet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.aurora.wallpapers.AuroraApplication;
import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.R;
import com.aurora.wallpapers.event.Event;
import com.google.android.material.navigation.NavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ApplySheet extends BaseBottomSheet {

    public static final String TAG = "APPLY_SHEET";

    @BindView(R.id.navigation_view)
    NavigationView navigationView;

    public ApplySheet() {
    }

    @Nullable
    @Override
    protected View onCreateContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sheet_apply, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void onContentViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onContentViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            stringExtra = bundle.getString(Constants.STRING_EXTRA);
            setupNavigationView();
        } else {
            dismissAllowingStateLoss();
        }
    }

    private void setupNavigationView() {
        navigationView.setNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_wallpaper:
                    AuroraApplication.getRelayBus().accept(new Event(Event.Type.WALLPAPER_ONLY, stringExtra));
                    break;
                case R.id.action_lockscreen:
                    AuroraApplication.getRelayBus().accept(new Event(Event.Type.LOCK_ONLY, stringExtra));
                    break;
                case R.id.action_both:
                    AuroraApplication.getRelayBus().accept(new Event(Event.Type.WALLPAPER_LOCK, stringExtra));
                    break;
                case R.id.action_crop:
                    AuroraApplication.getRelayBus().accept(new Event(Event.Type.CROP, stringExtra));
                    break;
            }
            dismissAllowingStateLoss();
            return false;
        });
    }
}
