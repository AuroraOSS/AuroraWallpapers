/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.ui.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.R;
import com.aurora.wallpapers.model.fastitems.WallItem;
import com.aurora.wallpapers.model.fastitems.decor.EqualSpacingItemDecoration;
import com.aurora.wallpapers.ui.sheet.FilterSheet;
import com.aurora.wallpapers.ui.view.ViewFlipper2;
import com.aurora.wallpapers.ui.viewmodel.SearchViewModel;
import com.aurora.wallpapers.utils.Util;
import com.aurora.wallpapers.utils.ViewUtil;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.gson.Gson;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.adapters.ItemAdapter;
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener;
import com.mikepenz.fastadapter.ui.items.ProgressItem;

import org.apache.commons.lang3.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends BaseActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    @BindView(R.id.action1)
    AppCompatImageView action1;
    @BindView(R.id.txt_search)
    TextView txtSearch;
    @BindView(R.id.action2)
    AppCompatImageView action2;
    @BindView(R.id.view_flipper)
    ViewFlipper2 viewFlipper;
    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.filter_fab)
    ExtendedFloatingActionButton filterFab;

    private String query;

    private SearchViewModel viewModel;
    private FastAdapter fastAdapter;
    private ItemAdapter<WallItem> itemAdapter;
    private ItemAdapter<ProgressItem> progressItemAdapter;
    private EndlessRecyclerOnScrollListener endlessScrollListener;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        sharedPreferences = Util.getPrefs(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);

        setupRecycler();

        viewModel = new ViewModelProvider(this).get(SearchViewModel.class);
        viewModel.getData().observe(this, wallItems -> {
            itemAdapter.add(wallItems);
            recyclerView.post(() -> {
                progressItemAdapter.clear();
            });
            if (itemAdapter != null && itemAdapter.getAdapterItems().size() > 0) {
                viewFlipper.switchState(ViewFlipper2.DATA);
            } else {
                viewFlipper.switchState(ViewFlipper2.EMPTY);
            }
        });

        onNewIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            String stringExtra = intent.getStringExtra(Constants.STRING_EXTRA);
            String typeExtra = intent.getStringExtra(Constants.TYPE_EXTRA);

            if (stringExtra != null) {
                query = stringExtra;

                if (typeExtra != null) {
                    if (typeExtra.equals("TAG")) {
                        query = "id:" + query;
                    }

                    if (typeExtra.equals("SIMILAR")) {
                        query = "like:" + query;
                    }

                    if (typeExtra.equals("UPLOADER")) {
                        query = "@" + query;
                    }
                }

                txtSearch.setText(StringUtils.joinWith(StringUtils.SPACE,
                        getString(R.string.search_results),
                        query));

                viewModel.fetchData(query);
            } else
                finishAfterTransition();
        } else
            finishAfterTransition();
    }

    @Override
    protected void onDestroy() {
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }

    @OnClick(R.id.action1)
    public void goBack() {
        onBackPressed();
    }

    @OnClick(R.id.filter_fab)
    public void showFilterDialog() {
        FilterSheet filterSheet = new FilterSheet();
        filterSheet.show(getSupportFragmentManager(), "FILTER");
    }

    private void setupRecycler() {
        fastAdapter = new FastAdapter<>();
        itemAdapter = new ItemAdapter<>();
        progressItemAdapter = new ItemAdapter<>();

        fastAdapter.addAdapter(0, itemAdapter);
        fastAdapter.addAdapter(1, progressItemAdapter);

        fastAdapter.setOnClickListener((view, iAdapter, item, position) -> {
            if (item instanceof WallItem) {
                Intent intent = new Intent(this, DetailsActivity.class);
                intent.putExtra(Constants.STRING_EXTRA, new Gson().toJson(((WallItem) item).getWall()));
                startActivity(intent, ViewUtil.getEmptyActivityBundle(this));
            }
            return false;
        });

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new EqualSpacingItemDecoration(16));

        endlessScrollListener = new EndlessRecyclerOnScrollListener(progressItemAdapter) {
            @Override
            public void onLoadMore(int currentPage) {
                recyclerView.post(() -> {
                    progressItemAdapter.clear();
                    progressItemAdapter.add(new ProgressItem());
                });
                viewModel.fetchData(query);
            }
        };
        recyclerView.addOnScrollListener(endlessScrollListener);
        recyclerView.setAdapter(fastAdapter);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case Constants.PREFERENCE_FILTER:
            case Constants.PREFERENCE_FILTER_ENABLED:
                itemAdapter.clear();
                endlessScrollListener.resetPageCount();
                viewModel.resetPage();
                viewModel.fetchData(query);
                break;
        }
    }
}
