/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.ui.sheet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.R;
import com.aurora.wallpapers.model.Filter;
import com.aurora.wallpapers.utils.PrefUtil;
import com.aurora.wallpapers.utils.Util;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.gson.Gson;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class FilterSheet extends BaseBottomSheet {

    public static final String TAG = "FILTER_SHEET";

    @BindView(R.id.switch_enabled)
    SwitchMaterial switchEnabled;
    @BindView(R.id.chip_resolutions)
    ChipGroup chipResolutions;
    @BindView(R.id.chip_ratios)
    ChipGroup chipRatios;
    @BindView(R.id.chip_category_anime)
    Chip chipCategoryAnime;
    @BindView(R.id.chip_category_general)
    Chip chipCategoryGeneral;
    @BindView(R.id.chip_category_people)
    Chip chipCategoryPeople;
    @BindView(R.id.chip_purity_sfw)
    Chip chipPuritySfw;
    @BindView(R.id.chip_purity_sketchy)
    Chip chipPuritySketchy;
    @BindView(R.id.chip_purity_nsfw)
    Chip chipPurityNsfw;
    @BindView(R.id.btn_clear_resolution)
    MaterialButton btnClearResolution;
    @BindView(R.id.btn_clear_ratio)
    MaterialButton btnClearRatio;

    private Filter filter;
    private Gson gson = new Gson();
    private String[] ratioArray = new String[]{};
    private String[] resolutionArray = new String[]{};

    public FilterSheet() {
    }

    @NonNull
    @Override
    public View onCreateContentView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sheet_filter, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void onContentViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onContentViewCreated(view, savedInstanceState);
        switchEnabled.setChecked(PrefUtil.getBoolean(requireContext(), Constants.PREFERENCE_FILTER_ENABLED));

        ratioArray = getResources().getStringArray(R.array.filter_ratios);
        resolutionArray = getResources().getStringArray(R.array.filter_resolutions);

        filter = gson.fromJson(PrefUtil.getString(requireContext(), Constants.PREFERENCE_FILTER), Filter.class);

        if (filter == null)
            filter = new Filter().getDefault();

        setupSingleFilters();
        setupMultipleChips();
    }

    @OnCheckedChanged(R.id.switch_enabled)
    public void enableFilter(SwitchMaterial switchMaterial) {
        PrefUtil.putBoolean(requireContext(), Constants.PREFERENCE_FILTER_ENABLED, switchMaterial.isChecked());
    }

    @OnClick(R.id.btn_positive)
    public void applyFilter() {
        saveRatios();
        saveResolutions();
        PrefUtil.putString(requireContext(), Constants.PREFERENCE_FILTER, gson.toJson(filter));
        dismissAllowingStateLoss();
    }

    @OnClick(R.id.btn_negative)
    public void closeFilter() {
        dismissAllowingStateLoss();
    }

    private void setupSingleFilters() {
        chipPuritySfw.setChecked(filter.isSfw());
        chipPuritySketchy.setChecked(filter.isSketchy());
        chipPurityNsfw.setChecked(filter.isNsfw());

        chipPuritySfw.setOnCheckedChangeListener((v, isChecked) -> filter.setSfw(isChecked));
        chipPuritySketchy.setOnCheckedChangeListener((v, isChecked) -> filter.setSketchy(isChecked));
        chipPurityNsfw.setOnCheckedChangeListener((v, isChecked) -> filter.setNsfw(isChecked));

        String apiKey = Util.getWallHavenAPIKey(requireContext());
        if (StringUtils.isNotEmpty(apiKey))
            chipPurityNsfw.setVisibility(View.VISIBLE);

        chipCategoryAnime.setChecked(filter.isAnime());
        chipCategoryGeneral.setChecked(filter.isGeneral());
        chipCategoryPeople.setChecked(filter.isPeople());

        chipCategoryAnime.setOnCheckedChangeListener((v, isChecked) -> filter.setAnime(isChecked));
        chipCategoryGeneral.setOnCheckedChangeListener((v, isChecked) -> filter.setGeneral(isChecked));
        chipCategoryPeople.setOnCheckedChangeListener((v, isChecked) -> filter.setPeople(isChecked));
    }

    @OnClick(R.id.btn_clear_resolution)
    public void clearResolutions() {
        for (Integer chipId : chipResolutions.getCheckedChipIds()) {
            ((Chip) chipResolutions.getChildAt(chipId)).setChecked(false);
        }
    }

    @OnClick(R.id.btn_clear_ratio)
    public void clearRatios() {
        for (Integer chipId : chipRatios.getCheckedChipIds()) {
            ((Chip) chipRatios.getChildAt(chipId)).setChecked(false);
        }
    }

    private void setupMultipleChips() {

        int i = 0;
        for (String ratio : ratioArray) {
            Chip chip = new Chip(requireContext());
            chip.setId(i);
            chip.setText(ratio);
            chip.setChecked(filter.getRatios().contains(ratioArray[i]));
            chipRatios.addView(chip);
            i++;
        }

        i = 0;
        for (String resolution : resolutionArray) {
            Chip chip = new Chip(requireContext());
            chip.setId(i);
            chip.setText(resolution);
            chip.setChecked(filter.getResolutions().contains(resolutionArray[i]));
            chipResolutions.addView(chip);
            i++;
        }
    }

    private void saveRatios() {
        final List<Integer> checkedIds = chipRatios.getCheckedChipIds();
        final List<String> newSelections = new ArrayList<>();
        for (Integer id : checkedIds) {
            newSelections.add(ratioArray[id]);
        }

        filter.getRatios().clear();
        filter.getRatios().addAll(newSelections);
    }

    private void saveResolutions() {
        final List<Integer> checkedIds = chipResolutions.getCheckedChipIds();
        final List<String> newSelections = new ArrayList<>();
        for (Integer id : checkedIds) {
            newSelections.add(resolutionArray[id]);
        }

        filter.getResolutions().clear();
        filter.getResolutions().addAll(newSelections);
    }
}
