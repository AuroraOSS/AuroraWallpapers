/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.task;

import android.content.Context;
import android.graphics.Bitmap;

import com.aurora.wallpapers.utils.Log;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileOutputStream;

public class BitmapExporter {

    private Context context;
    private Bitmap bitmap;

    public BitmapExporter(Context context, Bitmap bitmap) {
        this.context = context;
        this.bitmap = bitmap;
    }

    public boolean export() {
        try {
            final File file = new File(StringUtils.joinWith("/", context.getFilesDir(), "temp.png"));
            if (file.exists())
                file.delete();

            final FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            return true;
        } catch (Exception e) {
            Log.e(e.getMessage());
            return false;
        }
    }
}
