/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.utils.diff;

import com.aurora.wallpapers.model.fastitems.BingItem;
import com.mikepenz.fastadapter.diff.DiffCallback;

import org.jetbrains.annotations.Nullable;

public class BingDiffCallback implements DiffCallback<BingItem> {
    @Override
    public boolean areContentsTheSame(BingItem oldItem, BingItem newItem) {
        return oldItem.equals(newItem);
    }

    @Override
    public boolean areItemsTheSame(BingItem oldItem, BingItem newItem) {
        return oldItem.getWall().getStartdate().equals(newItem.getWall().getStartdate());
    }

    @Nullable
    @Override
    public Object getChangePayload(BingItem oldItem, int oldPosition, BingItem newItem, int newPosition) {
        return oldItem.getWall().getHsh().equals(newItem.getWall().getHsh());
    }
}
