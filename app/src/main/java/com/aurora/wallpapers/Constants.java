/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers;

public abstract class Constants {

    public static final String TAG = "Aurora Walls";
    public static final String SHARED_PREFERENCES_KEY = "com.aurora.wallpapers";
    public static final String UPDATE_URL = "https://gitlab.com/AuroraOSS/AuroraWallpapers/raw/master/updates.json";
    public static final String BING_BASE_URL = "https://www.bing.com/HPImageArchive.aspx/";

    public static final String FILE_JSON = "js";

    public static final String NOTIFICATION_CHANNEL_WALLPAPER = "NOTIFICATION_CHANNEL_WALLPAPER";

    public static final String INT_EXTRA = "INT_EXTRA";
    public static final String FLOAT_EXTRA = "FLOAT_EXTRA";
    public static final String STRING_EXTRA = "STRING_EXTRA";
    public static final String TYPE_EXTRA = "TYPE_EXTRA";
    public static final String WALLPAPER_EXTRA = "WALLPAPER_EXTRA";

    public static final String PREFERENCE_FILTER_ENABLED = "PREFERENCE_FILTER_ENABLED";
    public static final String PREFERENCE_FILTER = "PREFERENCE_FILTER";
    public static final String PREFERENCE_FAVOURITES = "PREFERENCE_FAVOURITES";
    public static final String PREFERENCE_THEME = "PREFERENCE_THEME";
    public static final String PREFERENCE_AUTO_WALLPAPER = "PREFERENCE_AUTO_WALLPAPER";
    public static final String PREFERENCE_AUTO_WALLPAPER_SOURCE = "PREFERENCE_AUTO_WALLPAPER_SOURCE";
    public static final String PREFERENCE_AUTO_WALLPAPER_TARGET = "PREFERENCE_AUTO_WALLPAPER_TARGET";
    public static final String PREFERENCE_UPDATES_INTERVAL = "PREFERENCE_UPDATES_INTERVAL";
    public static final String PREFERENCE_CLEAR_WALLPAPER = "PREFERENCE_CLEAR_WALLPAPER";
    public static final String PREFERENCE_GLIMPSE_WALLPAPER = "PREFERENCE_GLIMPSE_WALLPAPER";
    public static final String PREFERENCE_BING_CUSTOM = "PREFERENCE_BING_CUSTOM";
    public static final String PREFERENCE_BING_CUSTOM_REGION = "PREFERENCE_BING_CUSTOM_REGION";
    public static final String PREFERENCE_WALLHAVEN_API = "PREFERENCE_WALLHAVEN_API";

    public static final String FRAGMENT_NAME = "FRAGMENT_NAME";
    public static final String FRAGMENT_ABOUT = "FRAGMENT_ABOUT";
    public static final String FRAGMENT_FAVOURITE = "FRAGMENT_FAVOURITE";
    public static final String FRAGMENT_SETTING = "FRAGMENT_SETTING";
    public static final String FRAGMENT_BING = "FRAGMENT_BING";
}
